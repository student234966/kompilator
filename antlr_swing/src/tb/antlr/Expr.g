grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | conditionalStat NL -> conditionalStat
    | doWhileLoop NL -> doWhileLoop
    | whileLoop NL -> whileLoop
    | forLoop NL -> forLoop
    | NL ->
    ;


conditionalStat
    : IF^ LP! expr RP! THEN! (expr) (ELSE! (expr))?
    ;

expr
    : arithmeticExpr

      ( EQUAL^ arithmeticExpr
      | NOT_EQUAL^ arithmeticExpr)*
      ;

arithmeticExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF : 'if';

THEN : 'then';

ELSE : 'else';

EQUAL : '==';

NOT_EQUAL : '!=';

doWhilePetla
    :DO^ expr WHILE! expr
    ;

whilePetla
    :WHILE^ expr DO! expr
    ;


forPetla
    :FOR^ expr DO! expr
    ;

DO : 'do';

WHILE : 'while';

FOR : 'for';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
