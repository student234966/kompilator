tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numner = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> decl(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                      -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                      -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                      -> mult(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                      -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)                      -> assign(name={$i1.text}, value={$e2.st}) 
        | INT                                           -> int(i={$INT.text})
        | ID                                            -> id(n={$ID.text})
        | ^(EQUAL   e1=expr e2=expr)  -> isequal(p1={$e1.st}, p2={$e2.st})
        | ^(NOT_EQUAL   e1=expr e2=expr) -> notequal(p1={$e1.st}, p2={$e2.st})
        | ^(DO     e1=expr e2=expr) {numer++;}  -> do(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
        | ^(WHILE  e1=expr e2=expr) {numer++;}  -> while(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
        | ^(FOR    e1=expr e2=expr) {numer++;}  -> for(exp1={$e1.st}, exp2={$e2.st}, n={numer.toString()})
	| ^(IF e1=expr e2=expr e3=expr?) {number++;}    -> ifStatement(ifo={$e1.st}, theno={$e2.st}, elseo={$e3.st}, counter={number.toString()})

    ;

    